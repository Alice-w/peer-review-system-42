# **Mirror - Peer Review System**

## **You can view our app [here](https://peer-review-system-42.firebaseapp.com/home) !**

This project uses [Google Firebase web platform](https://firebase.google.com/)

To run our project locally, clone this repository.

Run `npm install` in the main folder directory (**peer review system**) and also in the **functions folder**

Please use `node  v11.9.0` or higher.

Run `ng s` to run the application locally and visit `[localhost:4200]`(localhost:4200) to explore.

#### Special thanks goes to: 

Our wonderful mentor - Valentin Anchev <3

Our devoted trainers - Stoyan Peshev and Rosen Urkov

#### What helped in our research and coding:

- [FireShip.io and Jeff Delaney](https://fireship.io/)
- [Maximilian Schwarzmüller](https://twitter.com/maxedapps?lang=en)
- [The official Firebase YouTube channel](https://www.youtube.com/user/Firebase)
- StackOverflow, Reddit and Medium



## **Project overview**

This project was built, using Google Firebase, as backend service and Angular 7 as front-end.

#### Additional packages used:

- [@angular/fire](https://www.youtube.com/user/Firebase)
- [ag-grid](https://www.ag-grid.com/)
- [ng-bootstrap](https://ng-bootstrap.github.io/#/home)
- [Angular multiselect](https://cuppalabs.github.io/components/multiselectDropdown/)
- [ngx-toastr](https://www.npmjs.com/package/ngx-toastr)

#### Styling:

- [Bootstrap](https://getbootstrap.com/)
- [FontAwesome](https://fontawesome.com/)
- [Sass](https://sass-lang.com/)

## Application flow

Once you enter the provided website/localhost:4200/mobile app you can visit the homepage, sign in and sign up pages.

![](https://i.imgur.com/bjRJWQs.jpg)

All other content is only available for registered users.

You have two options to sign up - email and password or Google login. 

![](https://i.imgur.com/7T8XK93.png)


Once you've chosen your preferred method of registration, you can sign in. 

If you have chosen Email/Password registration, you'll receive an email to confirm your email address.

You can now sign in.

![](https://i.imgur.com/er7TR1b.png)


Once signed in, you have full access of the functionality - create your own team, submit review requests and trigger the review process. If you get assigned to a certain team you can now add new people to it or leave it if you wish.

![](https://i.imgur.com/6T5THtY.jpg)

![](https://i.imgur.com/pEhU1er.png)

![](https://i.imgur.com/dWjQhzj.png)
Have fun exploring our app!

*We put a lot of work, passion and emotions in it!* 

You can contact us at  `starleenlp[at]gmail` and `yo.raichinova[at]gmail`

