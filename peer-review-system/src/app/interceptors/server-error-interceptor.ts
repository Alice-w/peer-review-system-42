import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../services/auth.service';

@Injectable()
export class ServerErrorInterceptor implements HttpInterceptor {
  public constructor(
    private readonly router: Router,
    private readonly toastr: ToastrService,
    private readonly authService: AuthService,
  ) {}
  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: any) => {
        if (error.status === 401) {
          this.router.navigate(['/auth/signin']);
          this.toastr.error(
            'You should be signed-in in order to do this!'
          );
        } else
          if (error.status === 403) {
          this.router.navigate(['/home']);
          this.toastr.error('You are not authorized to do this!');
        } else if (error.status === 404) {
          this.router.navigate(['/not-found']);
          this.toastr.error('Resource not found!');
        } else if (error.status >= 500) {
          this.router.navigate(['/server-error']);
          this.toastr.error('Oops.. something went wrong.. :(');
        }

        return throwError(error);
      })
    );
  }
}
