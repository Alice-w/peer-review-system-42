import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { TeamsService } from 'src/app/services/teams.service';
import { UsersService } from 'src/app/services/users.service';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { Team } from 'src/app/models/team';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { slider, fader } from 'src/app/shared/animations/route-animations';

@Component({
  selector: 'app-update-team',
  animations: [slider],
  templateUrl: './update-team.component.html',
  styleUrls: ['./update-team.component.scss']
})
export class UpdateTeamComponent implements OnInit {

  public displayName: string;
  public userId: string;
  public teams;
  public teamValue;
  public selectedTeam;
  public rowData: Observable<any>;
  public rowSelection;
  private gridApi;
  private gridColumnApi;
  public selectedMembers;
  public selectedIds: string[];
  public updateForm;

  columnDefs = [
    {
      headerName: 'Avatar', field: 'photoURL', sortable: false, filter: false,
      cellRenderer: (params) => `<img src="${params.value}"
    class="img-responsive img-thumbnail rounded-circle avatar" width="75" height="75">`},
    {
      headerName: 'DisplayName', field: 'displayName', sortable: true, filter: true,
      cellRenderer: 'agGroupCellRenderer'
    },
    { field: 'uid', hide: true }
  ];

  constructor(
    public auth: AuthService,
    public usersService: UsersService,
    public teamsService: TeamsService,
    public afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private router: Router
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.displayName = user.displayName;
        this.userId = user.uid;
        this.teams = this.usersService.getUserTeams(this.userId);
      }
    });
  }

  ngOnInit() {

    this.updateForm = this.teamsService.form;
    this.rowSelection = 'multiple';
    this.rowData = this.afs.collection('users').valueChanges();
  }


  public setTeamValue(selectedTeam) {
    this.teamValue = this.selectedTeam;
  }

  public onSelectionChanged(event) {
    const selectedRows = this.gridApi.getSelectedRows();
    let selectedRowsString = '';
    selectedRows.forEach((selectedRow, index) => {
      // if (index > 5) {
      //   return;
      // }
      if (index !== 0) {
        selectedRowsString += ', ';
      }
      selectedRowsString += selectedRow.displayName;
      this.selectedMembers = selectedRows;
      this.selectedIds = this.selectedMembers.map(el => el.uid);
      return this.selectedIds;
    });
    if (selectedRows.length >= 5) {
      selectedRowsString += ' - and ' + (selectedRows.length - 5) + ' others';
    }
    document.querySelector('#selectedRows').innerHTML = selectedRowsString;
  }

  public onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

  onSubmit() {
    const members = this.selectedIds;
    this.teamsService.updateTeam(this.teamValue, members);
  }
}
