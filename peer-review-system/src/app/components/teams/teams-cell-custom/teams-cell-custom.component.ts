import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { TeamsService } from 'src/app/services/teams.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-teams-cell-custom',
  templateUrl: './teams-cell-custom.component.html',
  styleUrls: ['./teams-cell-custom.component.scss']
})
export class TeamsCellCustomComponent {
  data: any;
  params: any;


  constructor(public teamsService: TeamsService, private toastr: ToastrService) { }

  teamValue: string;
  agInit(params) {
    this.params = params;
  }

  addToTeam(teamId) {
    const rowData = this.params;
    console.log(this.teamValue);
    // this.teamsService.updateTeam

    // let i = rowData.rowIndex;
    // this.usersService.makeAdmin(rowData.data.uid);
  }

  removeFromTeam() {
    const rowData = this.params;
    const userId = rowData.data.uid;
    this.teamsService.removeUserFromTeam(userId, this.teamsService.selectedTeam);
    this.teamsService.userLeavesTeam(userId, this.teamsService.selectedTeam);
    this.toastr.success('This user has been removed from the team');
  }
}
