import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamsCellCustomComponent } from './teams-cell-custom.component';

describe('TeamsCellCustomComponent', () => {
  let component: TeamsCellCustomComponent;
  let fixture: ComponentFixture<TeamsCellCustomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamsCellCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsCellCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
