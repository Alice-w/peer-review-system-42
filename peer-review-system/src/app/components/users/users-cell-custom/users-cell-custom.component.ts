import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-users-cell-custom',
  templateUrl: './users-cell-custom.component.html',
  styleUrls: ['./users-cell-custom.component.scss']
})
export class UsersCellCustomComponent {
  data: any;
  params: any;

  constructor(public usersService: UsersService) { }

  agInit(params) {
    this.params = params;
    this.data = params.value;
  }

  addAdmin() {
    const rowData = this.params;
    this.usersService.makeAdmin(rowData.data.uid);
  }

  deleteUser() {
    const rowData = this.params;
    const userId = rowData.data.uid;
    rowData.data.teams.map(el => this.usersService.deleteUser(userId, el));
  }
}
