import { UsersService } from 'src/app/services/users.service';
import { Routes, RouterModule, RouterOutlet } from '@angular/router';
import { SharedModule } from '../../../app/shared/shared.module';
import { UsersComponent } from './users.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { NgModule } from '@angular/core';
import { AuthGuardComponent } from 'src/app/shared/guards/auth-guard.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent, data: { title: 'Dashboard', animation: 'isLeft' },
  canActivate: [AuthGuardComponent] },
  { path: 'all', component: UsersComponent, data: { title: 'All users' },
  canActivate: [AuthGuardComponent] },
  { path: ':uid', component: UserProfileComponent, data: { title: 'User Profile' },
   canActivate: [AuthGuardComponent] },
  ];

@NgModule({
  declarations: [UsersComponent, UserProfileComponent, DashboardComponent],
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class UsersModule {
  constructor(usersService: UsersService) {}
}
