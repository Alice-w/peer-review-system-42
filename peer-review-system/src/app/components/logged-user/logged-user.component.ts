import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-logged-user',
  templateUrl: './logged-user.component.html',
  styleUrls: ['./logged-user.component.scss']
})
export class LoggedUserComponent implements OnInit {

  constructor(public auth: AuthService,
              public userService: UsersService) { }

  ngOnInit() {
  }

  get userData() {
    return this.auth.userData;
  }

}
