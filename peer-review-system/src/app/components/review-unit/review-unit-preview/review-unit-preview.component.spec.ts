import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewUnitPreviewComponent } from './review-unit-preview.component';

describe('ReviewUnitPreviewComponent', () => {
  let component: ReviewUnitPreviewComponent;
  let fixture: ComponentFixture<ReviewUnitPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewUnitPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewUnitPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
