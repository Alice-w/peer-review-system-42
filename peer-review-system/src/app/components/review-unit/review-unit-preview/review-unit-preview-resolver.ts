import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ReviewUnitService } from 'src/app/services/review-unit.service';
import { Observable } from 'rxjs';
import { ReviewUnit } from 'src/app/models/review-unit';
import { first } from 'rxjs/operators';


@Injectable()
export class ReviewUnitResolver implements Resolve<any> {

    constructor(public revUnitService: ReviewUnitService) { }
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> {

        return this.revUnitService
            .getReviewUnitById(route.params.uid).pipe(
                first());
    }
}
