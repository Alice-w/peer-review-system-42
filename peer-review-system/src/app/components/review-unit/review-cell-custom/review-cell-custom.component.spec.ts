import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewCellCustomComponent } from './review-cell-custom.component';

describe('ReviewCellCustomComponent', () => {
  let component: ReviewCellCustomComponent;
  let fixture: ComponentFixture<ReviewCellCustomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewCellCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewCellCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
