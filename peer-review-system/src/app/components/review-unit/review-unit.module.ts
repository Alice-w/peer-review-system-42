import { NgModule } from '@angular/core';
import { ReviewUnitPreviewComponent } from './review-unit-preview/review-unit-preview.component';
import { Routes, RouterModule } from '@angular/router';
import { ReviewUnitsComponent } from './review-units/review-units.component';
import { ReviewUnitCreateComponent } from './review-unit-create/review-unit-create.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReviewUnitResolver } from './review-unit-preview/review-unit-preview-resolver';
import { UploaderComponent } from './uploader/uploader.component';
import { UploadTaskComponent } from './upload-task/upload-task.component';

import { NgSelectModule } from '@ng-select/ng-select';
import { AuthGuardComponent } from 'src/app/shared/guards/auth-guard.component';
import { DropZoneDirective } from './directives/dropzone.directive';

const routes: Routes = [
  { path: 'all', component: ReviewUnitsComponent, data: { title: 'Review Units' }, canActivate: [AuthGuardComponent] },
  { path: 'new', component: ReviewUnitCreateComponent, data: { title: ' Review Unit' }, canActivate: [AuthGuardComponent] },
  { path: ':uid', component: ReviewUnitPreviewComponent, resolve: { data: ReviewUnitResolver }, canActivate: [AuthGuardComponent] },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule, NgSelectModule
  ],
  declarations: [ReviewUnitsComponent, ReviewUnitPreviewComponent,
    ReviewUnitCreateComponent, DropZoneDirective, UploaderComponent, UploadTaskComponent],
})
export class ReviewUnitModule { }
