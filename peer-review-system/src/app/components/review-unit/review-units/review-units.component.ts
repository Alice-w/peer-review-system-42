import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';
import { ReviewUnitService } from 'src/app/services/review-unit.service';
import { TimestampComponent } from 'src/app/shared/timestamp/timestamp.component';
import { ReviewCellCustomComponent } from '../review-cell-custom/review-cell-custom.component';
import { slider, fader } from 'src/app/shared/animations/route-animations';

@Component({
  selector: 'app-review-units',
  animations: [slider],
  templateUrl: './review-units.component.html',
  styleUrls: ['./review-units.component.scss']
})

export class ReviewUnitsComponent implements OnInit {
  columnDefs = [
    {
      headerName: 'Title', field: 'title', sortable: true, filter: true,
      cellRenderer: (params) => `<a href="/review-units/${params.data.uid}">${params.data.title}</a>`
    },
    { headerName: 'Status', field: 'status', sortable: true, filter: true },
    { headerName: 'Owner', field: 'ruOwner.displayName', sortable: true, filter: true
    },
    {
      headerName: 'Date', field: 'date', cellRendererFramework: TimestampComponent,
      sortable: true, filter: 'agDateColumnFilter'
    },
    {headerName: 'Actions', field: 'action', cellRendererFramework: ReviewCellCustomComponent }];

  ruOwner;
  rowData: Observable<any>;
  private gridApi;
  private gridColumnApi;

  constructor(
    public afs: AngularFirestore,
    private ruService: ReviewUnitService
  ) { }

  ngOnInit() {
    this.rowData = this.ruService.getAllReviewUnits();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

}
