import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewUnitCreateComponent } from './review-unit-create.component';

describe('ReviewUnitCreateComponent', () => {
  let component: ReviewUnitCreateComponent;
  let fixture: ComponentFixture<ReviewUnitCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewUnitCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewUnitCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
