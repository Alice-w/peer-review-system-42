import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { ReviewUnitService } from 'src/app/services/review-unit.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { User } from 'src/app/models/user';
import { UsersService } from 'src/app/services/users.service';
import { Observable } from 'rxjs';
import { TeamsService } from 'src/app/services/teams.service';
import { flatMap } from 'rxjs/operators';
import { slider, fader } from 'src/app/shared/animations/route-animations';
@Component({
  selector: 'app-review-unit-create',
  templateUrl: './review-unit-create.component.html',
  styleUrls: ['./review-unit-create.component.scss'],
  animations: [slider],
})
export class ReviewUnitCreateComponent implements OnInit {

  public revUnitForm;
  public reviewUnits = [];
  public data: {};
  public userId: string;
  public userEmail: string;
  public displayName: string;
  public dropdownList = [];
  public dropdownSettings = {};
  public userData: User;
  public teams;
  public selectedTeam;
  public teamValue;
  public rowSelection;
  public  gridApi;
  private gridColumnApi;
  private gridOptions;
  public selectedMembers = [];
  public selectedIds = [];
  public rowData: Observable<any>;

  columnDefs = [
    {
      headerName: 'Avatar', field: 'photoURL', sortable: false, filter: false,
      cellRenderer: (params) => `<img src="${params.value}"
    class="img-responsive img-thumbnail rounded-circle avatar" width="75" height="75">`},
    {
      headerName: 'DisplayName', field: 'displayName', sortable: true, filter: true
    },
    { field: 'uid', hide: true }
  ];



  constructor(
    public auth: AuthService,
    public revUnitService: ReviewUnitService,
    public usersService: UsersService,
    public teamsService: TeamsService,
    public afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private router: Router
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.displayName = user.displayName;
        this.userId = user.uid;
        this.userEmail = user.email;
        this.teams = this.usersService.getUserTeams(this.userId);
      }
    });
  }

  ngOnInit() {
    this.revUnitForm = this.revUnitService.form;
    this.dropdownList = [
      { id: 1, itemName: 'article' },
      { id: 2, itemName: 'code review' },
      { id: 3, itemName: 'diagram' },
      { id: 4, itemName: 'document' },
      { id: 5, itemName: 'forecast' },
      { id: 6, itemName: 'scheme' },
      { id: 7, itemName: 'important' },
      { id: 8, itemName: 'projection' },
      { id: 9, itemName: 'urgent' },
      { id: 10, itemName: 'editorial' }
    ];

    this.dropdownSettings = {
      text: 'Select tags',
      singleSelection: false,
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      enableSearchFilter: true,
      classes: 'myclass custom-class custom-select',
    };
  }

  onSelectionChanged() {
    const selectedRows = this.gridApi.getSelectedRows();
    let selectedRowsString = '';
    selectedRows.forEach((selectedRow, index) => {
      // if (index > 5) {
      //   return;
      // }
      if (index !== 0) {
        selectedRowsString += ', ';
      }
      selectedRowsString += selectedRow.displayName;
      this.selectedMembers = selectedRows;
      this.selectedIds = this.selectedMembers.map(el => el.uid);
      return this.selectedIds;
    });
    if (selectedRows.length >= 5) {
      selectedRowsString += ' - and ' + (selectedRows.length - 5) + ' others';
    }
    document.querySelector('#selectedRows').innerHTML = selectedRowsString;
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

  public setTeamValue(selectedTeam) {
    this.teamValue = this.selectedTeam;
    this.rowData = this.teamsService.getTeamMembers(this.teamValue);
    this.rowSelection = 'multiple';
  }

  resetFields() {
    this.revUnitForm = this.revUnitService.form;
  }
  onSubmit() {
    const reviewUnit = this.revUnitService.form.value;
    const date = firebase.firestore.FieldValue.serverTimestamp();
    this.revUnitService.createReviewUnit(reviewUnit, this.userId, this.displayName, this.userEmail, date, this.selectedIds, this.teamValue);
    this.resetFields();
  }

}
