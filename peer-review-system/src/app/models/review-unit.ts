export interface ReviewUnit {
    uid?: string;
    title?: string;
    content?: string;
    tags?: [];
    ruOwner?: {};
    reviewers?: [];
    comments?: [];
    status?: string;
    date?: Date;
    team?: string;
}
