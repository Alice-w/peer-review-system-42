export interface ReviewUnitPreview {
    uid: string;
    content: string;
    author: string;
    date: Date;
}
