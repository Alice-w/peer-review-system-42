export interface User {
    uid: string;
    email: string;
    displayName: any;
    photoURL: any;
    teams: [];
    role: string;
}
