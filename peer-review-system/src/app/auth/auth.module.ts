import { NgModule } from '@angular/core';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { SendEmailComponent } from '../components/send-email/send-email.component';

const routes: Routes = [
  { path: 'signin', component: SigninComponent, data: { title: 'Sign in', animation: 'isLeft' } },
  { path: 'signup', component: SignupComponent, data: { title: 'Sign up', animation: 'isRight' } },
  { path: 'reset-password', component: ResetPasswordComponent, data: { title: 'Reset password' } },
  { path: 'send-email', component: SendEmailComponent, data: { title: 'Send email' } }

];

@NgModule({
  imports: [RouterModule.forChild(routes),
    SharedModule],
  declarations: [SigninComponent, SignupComponent, ResetPasswordComponent, SendEmailComponent],
})
export class AuthModule { }
