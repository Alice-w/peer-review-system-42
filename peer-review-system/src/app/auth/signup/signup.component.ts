import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { slider, fader } from 'src/app/shared/animations/route-animations';

@Component({
  selector: 'app-signup',
  animations: [slider],
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  public signUpForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    public auth: AuthService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.signUpForm = this.formBuilder.group({
      displayName: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(20)]],
      email: ['', [Validators.email, Validators.required]],
      password: ['',
        [
          Validators.required,
          // Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
          Validators.minLength(7),
          Validators.maxLength(25)
        ],
      ],
      confirm: ['', Validators.required],
    }
    );
  }

  ngOnInit() {
  }

  get displayName() {
    return this.signUpForm.get('displayName');
  }

  get email() {
    return this.signUpForm.get('email');
  }

  get password() {
    return this.signUpForm.get('password');
  }

  public isPasswordMatch() {
    const val = this.signUpForm.value;
    return val && val.password && val.password === val.confirm;
  }

  public signUp() {
    return this.auth.signUp(this.displayName.value, this.email.value, this.password.value);
  }

}
