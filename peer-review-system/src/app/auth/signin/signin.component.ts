import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';
import { ToastrService } from 'ngx-toastr';
import { slider, fader } from 'src/app/shared/animations/route-animations';


@Component({
  selector: 'app-signin',
  animations: [slider],
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  signInForm: FormGroup;
  hide: true;

  constructor(
    public formBuilder: FormBuilder,
    public auth: AuthService,
    public usersService: UsersService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.signInForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['',
        [
          Validators.required,
          // Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
          Validators.minLength(7),
          Validators.maxLength(25)
        ]
      ]
    });
  }

  get email() {
    return this.signInForm.get('email');
  }

  get password() {
    return this.signInForm.get('password');
  }

  signIn() {
    this.auth.emailSignIn(this.email.value, this.password.value);
  }

}
