import {
  trigger,
  transition,
  style,
  query,
  group,
  animateChild,
  animate,
  keyframes,
} from '@angular/animations';

export const fader =
trigger('routeAnimations', [
  transition('* <=> *', [
    // Set a default  style for enter and leave
    query(':enter, :leave', [
      style({
        position: 'absolute',
        left: 0,
        width: '100%',
        opacity: 0,
        transform: 'scale(0) translateY(100%)',
      }),
    ]),
    // Animate the new page in
    query(':enter', [
      animate('600ms ease', style({ opacity: 0.7, transform: 'scale(0.7) translateY(0)' })),
    ])
  ]),
]);



const optional = { optional: true };
const toTheRight = [
query(':enter, :leave', [
  style({
    position: 'absolute',
    top: 130,
    right: 0,
    width: '100%',
  })
], optional),
query(':enter', [
  style({ right: '-100%',  })
]),
group([
  query(':leave', [
    animate('750ms ease', style({ right: '100%', }))
  ], optional),
  query(':enter', [
    animate('750ms ease', style({ right: '0%'}))
  ])
]),
];

const toTheLeft = [
query(':enter, :leave', [
  style({
    position: 'absolute',
    top: 130,
    left: 0,
    width: '100%',
  })
], optional),
query(':enter', [
  style({ right: '-100%',  })
]),
group([
  query(':leave', [
    animate('750ms ease', style({ left: '100%', }))
  ], optional),
  query(':enter', [
    animate('750ms ease', style({ left: '0%'}))
  ])
]),
];

export const slider =
trigger('routeAnimations', [
  transition('* => isLeft', toTheLeft),
  transition('* => isRight', toTheRight),
  transition('isRight => *', toTheLeft),
  transition('isLeft => *', toTheRight),
]);
