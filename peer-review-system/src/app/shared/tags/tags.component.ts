import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'app-tags',
    template: `{{ params.tags | keyvalue }}`
})

export class TagsComponent implements ICellRendererAngularComp {
    public params: any;

    agInit(params: any): void {
        this.params = params.data;
        console.log(this.params.tags);
        const tags = this.params.tags.map(tag => tag);

        // return this.params.data.tags.forEach(tag => {
        //     //console.log(tag.itemName);
        //     // this.tag = tag.itemName;
        //      tag.itemName;
        //     // return '<p class="badge badge-pill badge-primary">' + tag.itemName + '</p>';
        //     });
    }

    refresh(): boolean {
        return false;
    }
}
