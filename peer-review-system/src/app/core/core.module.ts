import { NgModule } from '@angular/core';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthService } from '../services/auth.service';
import { UsersService } from '../services/users.service';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { MessagingService } from '../services/messaging.service';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import { ReviewUnitService } from '../services/review-unit.service';
import { ReviewUnitResolver } from '../components/review-unit/review-unit-preview/review-unit-preview-resolver';
import { TeamsService } from '../services/teams.service';
import { ToastrService } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [],
  providers: [AuthService, UsersService, MessagingService, ReviewUnitService, TeamsService, ToastrService, ReviewUnitResolver],
  imports: [
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireFunctionsModule,
    BrowserAnimationsModule]
})
export class CoreModule { }
