import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Md5 } from 'ts-md5/dist/md5';
import { User } from '../models/user';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser$: Observable<User | null>;
  userData;
  userRole;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private toastr: ToastrService,
    private router: Router
  ) {
    /* Saving user data in localstorage when
        logged in and setting up null when logged out */
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    });
    this.currentUser$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }

  async signUp(username: string, email: string, password: string) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then(result => {
        result.user.updateProfile({
          displayName: username,
        }).then(() => {
          this.sendVerificationMail();
          this.updateUserData(result.user);
          this.toastr.success(`Successful registration!`);
          this.router.navigate(['/home']);
        });
      }).catch((error) => {
        this.toastr.error(error.message);
      });
  }

  async sendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
      .then(() => {
        this.toastr.success(`A verification email has been sent. Please check your inbox!`);
      });
  }

  // Returns true when user is logged in and email is verified
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
// return (user !== null && user.emailVerified !== false);
    return (user !== null);
  }

  public async googleSignIn() {
    const provider = new auth.GoogleAuthProvider();
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    return this.updateUserData(credential.user).then(() => {
      this.toastr.success(`You've successfully logged in with Google`);
      this.router.navigate(['/home']);
    });
  }

  public emailSignIn(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password).then(() => {
      this.toastr.success(`You've successfully signed in`);
      this.router.navigate(['/home']);
    })
      .catch(error => this.toastr.error(error.message));
  }

  public signOut() {
    return this.afAuth.auth.signOut().then(() => {
      localStorage.removeItem('user');
      this.toastr.success(`You have successfully signed out!`);
      window.location.href = 'auth/signin';
      // this.router.navigate(['auth/signin']);
    });
  }

  public updateUserData(user) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);
    const data: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL ||
        'https://www.gravatar.com/avatar/' + Md5.hashStr(user.uid) + '?d=identicon',
      teams: [],
      role: user.role = 'member'
    };
    return userRef.set(data, { merge: true });
  }
}
