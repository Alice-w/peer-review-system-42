import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { mergeMap, switchMap, filter } from 'rxjs/operators';
import { take } from 'rxjs/operators';
import { BehaviorSubject, of } from 'rxjs';
import { AngularFirestore, Action } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class MessagingService {

  // currentMessage = new BehaviorSubject(null);
  // private messageSource = new Subject()
  // currentMessage = this.messageSource.asObservable() // message observable to show in Angular component
  private messaging: firebase.messaging.Messaging = firebase.messaging();
  public currentMessage: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private angularFireMessaging: AngularFireMessaging,
    private toastr: ToastrService
  ) {
    this.angularFireMessaging.messaging.subscribe(
      (messaging) => {
        messaging.onMessage = messaging.onMessage.bind(messaging);
        messaging.onTokenRefresh = messaging.onTokenRefresh.bind(messaging);
      }
    );

  }

  /**
   * update token in firebase database
   *
   * @param userId userId as a key
   * @param token token as a value
   */

  public updateToken(token: string): void {
    const addToken = (actions: Action<firebase.firestore.DocumentSnapshot>, currentTokens: string[]) => {
      actions.payload.ref
        .set({
          token: currentTokens,
        })
        .then(() => {
        });
    };

    this.afAuth.authState
      .pipe(
        switchMap(user => {
          if (!user) {
            return of(null);
          }
          return this.afs
            .collection('fcmTokens/')
            .doc(user.uid)
            .snapshotChanges();
        }),
        filter((action: Action<firebase.firestore.DocumentSnapshot>) => !!action)
      )

      .subscribe((actions: Action<firebase.firestore.DocumentSnapshot>) => {
        if (!actions.payload.data()) {
          addToken(actions, [token]);
          return;
        }
        const currentTokens: string[] = actions.payload.data().token;
        const index: number = currentTokens.findIndex((currentToken: string) => currentToken === token);
        if (index === -1) {
          currentTokens.push(token);
          addToken(actions, currentTokens);
        }

      });
  }

  public refreshToken(): void {
    this.messaging.onTokenRefresh(() => {
      this.messaging.getToken().then((refreshedToken: string) => {
        this.updateToken(refreshedToken);
      });
    });
  }

  /**
   * request permission for notification from firebase cloud messaging
   *
   * @param userId userId
   */
  public requestPermission(): void {
    if ('Notification' in window && Notification.permission === 'default') {
      this.showPermission();
    } else if ('Notification' in window && Notification.permission === 'granted') {
      this.showPermission();
    }

  }

  /**
   * hook method when new notification received in foreground
   */
  private showPermission(): void {
    Notification
      .requestPermission()
      .then(() => {
        return this.messaging.getToken();
      })
      .then((token: string) => {
        this.updateToken(token);
      })
      .catch(err =>
        this.toastr.error(err.message)
      );
  }

  public receiveMessage(): void {
    this.messaging.onMessage(payload => {
      this.currentMessage.next(payload);
    });
  }


  deleteToken() {
    this.angularFireMessaging.getToken
      .pipe(mergeMap(token => this.angularFireMessaging.deleteToken(token)))
      .subscribe(
        (token) => { console.log('Deleted!'); },
      );
  }

  listen() {
    this.angularFireMessaging.messages
      .subscribe((message) => { console.log(message); });
  }
}
