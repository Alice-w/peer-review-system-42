import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from '../models/user';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Team } from '../models/team';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { AuthService } from './auth.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {
  currentUser$: Observable<User | null>;
  usersRef = this.afs.firestore.collection('users');
  public userId;
  public teams;
  public teamsCollection: AngularFirestoreCollection<Team>;
  public form: FormGroup;
  public selectedMembers;
  public teamsArr;
  newTeam;
  user$;
  public teamToUpdate;
  public newMembers;
  public selectedTeam: string = null;

  constructor(
    public formBuilder: FormBuilder,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private toastr: ToastrService,
    private router: Router,
    private authService: AuthService
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userId = user.uid;
      }
    });

    this.form = this.formBuilder.group({
      teamName: ['',
        [
          Validators.required,
          Validators.minLength(5)],
      ]
    });

    this.teamsCollection = this.afs.collection<Team>('teams');
  }

  public async createTeam(team, members) {
    this.newTeam = await this.afs.collection('teams').add({
      teamName: team.teamName,
      members
    });
    this.newTeam.update({ uid: this.newTeam.id });
    members.forEach((member) => {
      this.afs.collection('users').doc(member).update({
        teams: firebase.firestore.FieldValue.arrayUnion(this.newTeam.id)
      }).then(() => {
        this.toastr.success('You have successfully created a team!');
        this.router.navigate(['users/dashboard']);
      }
      ).catch(error => this.toastr.error(error.message));
    });
  }

  public userLeavesTeam(userId, teamId) {
    const userQuery = this.afs.collection('users', ref =>
      ref.where('uid', '==', userId)
        .where('teams', 'array-contains', teamId));

    return userQuery.doc(userId).update({
      teams: firebase.firestore.FieldValue.arrayRemove(teamId)
    }).catch(console.error);
  }

  public removeUserFromTeam(userId, teamId) {
    const teamQuery = this.afs.collection('teams', ref =>
      ref.where('uid', '==', teamId)
        .where('members', 'array-contains', userId));

    return teamQuery.doc(teamId).update({
      members: firebase.firestore.FieldValue.arrayRemove(userId)
    }).catch(console.error);
  }

  public updateTeam(teamId, newMembers) {
    this.teamToUpdate = this.afs.collection('teams', ref => ref.where('uid', '==', teamId));

    newMembers.map(member => {
      this.teamToUpdate.doc(teamId).update({
        members: firebase.firestore.FieldValue.arrayUnion(member)
      });

      this.afs.collection('users').doc(member).update({
        teams: firebase.firestore.FieldValue.arrayUnion(teamId)
      }).then(() => {
        this.toastr.success('You have successfully updated a team!');
        this.router.navigate(['users/dashboard']);
      }
      ).catch(error => this.toastr.error(error.message));
    });
  }

  public AdminAddsMemberToTeam(userId, teamId) {
    this.teamToUpdate = this.afs.collection('teams', ref => ref.where('uid', '==', teamId));
    this.teamToUpdate.doc(teamId).update({
      members: firebase.firestore.FieldValue.arrayUnion(userId)});
    this.afs.collection('users').doc(userId).update({
        teams: firebase.firestore.FieldValue.arrayUnion(teamId)
      }).then(() => {
        this.toastr.success('You have successfully updated a team!');
        this.router.navigate(['users/dashboard']);
      }
      ).catch(error => this.toastr.error(error.message));
  }

  public getAllTeams() {
    return this.afs.collection('teams').valueChanges();
  }

  public getTeamMembers(teamId) {
    return this.afs.collection('users', ref => ref.where('teams', 'array-contains', teamId)).valueChanges();
  }

  public deleteTeam(teamId) {
    return this.afs.collection('teams').doc(teamId).delete(); // this will delete a team :D
  }

}
