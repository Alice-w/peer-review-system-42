'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">peer-review-system documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-5b7d568c0f5909fe3bcf1f834879d194"' : 'data-target="#xs-components-links-module-AppModule-5b7d568c0f5909fe3bcf1f834879d194"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-5b7d568c0f5909fe3bcf1f834879d194"' :
                                            'id="xs-components-links-module-AppModule-5b7d568c0f5909fe3bcf1f834879d194"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FooterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HomeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LoggedUserComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoggedUserComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NotFoundComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotFoundComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServerErrorComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ServerErrorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link">AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AuthModule-8a701f81b80e5f03e57cdca18affab81"' : 'data-target="#xs-components-links-module-AuthModule-8a701f81b80e5f03e57cdca18affab81"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AuthModule-8a701f81b80e5f03e57cdca18affab81"' :
                                            'id="xs-components-links-module-AuthModule-8a701f81b80e5f03e57cdca18affab81"' }>
                                            <li class="link">
                                                <a href="components/ResetPasswordComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ResetPasswordComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SendEmailComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SendEmailComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SigninComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SigninComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SignupComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SignupComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link">CoreModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CoreModule-0a1a1a7ed1da1c5fc31063f6d4f0bf34"' : 'data-target="#xs-injectables-links-module-CoreModule-0a1a1a7ed1da1c5fc31063f6d4f0bf34"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CoreModule-0a1a1a7ed1da1c5fc31063f6d4f0bf34"' :
                                        'id="xs-injectables-links-module-CoreModule-0a1a1a7ed1da1c5fc31063f6d4f0bf34"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MessagingService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MessagingService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ReviewUnitService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ReviewUnitService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/TeamsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>TeamsService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UsersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ReviewUnitModule.html" data-type="entity-link">ReviewUnitModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ReviewUnitModule-059a6ffa673fbdc4338b89df4fd35f30"' : 'data-target="#xs-components-links-module-ReviewUnitModule-059a6ffa673fbdc4338b89df4fd35f30"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ReviewUnitModule-059a6ffa673fbdc4338b89df4fd35f30"' :
                                            'id="xs-components-links-module-ReviewUnitModule-059a6ffa673fbdc4338b89df4fd35f30"' }>
                                            <li class="link">
                                                <a href="components/ReviewUnitCreateComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ReviewUnitCreateComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ReviewUnitPreviewComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ReviewUnitPreviewComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ReviewUnitsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ReviewUnitsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UploadTaskComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UploadTaskComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UploaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UploaderComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-ReviewUnitModule-059a6ffa673fbdc4338b89df4fd35f30"' : 'data-target="#xs-directives-links-module-ReviewUnitModule-059a6ffa673fbdc4338b89df4fd35f30"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-ReviewUnitModule-059a6ffa673fbdc4338b89df4fd35f30"' :
                                        'id="xs-directives-links-module-ReviewUnitModule-059a6ffa673fbdc4338b89df4fd35f30"' }>
                                        <li class="link">
                                            <a href="directives/DropZoneDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">DropZoneDirective</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link">SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-f036d0961d87d0a5b6d4903a4eaeb6d0"' : 'data-target="#xs-components-links-module-SharedModule-f036d0961d87d0a5b6d4903a4eaeb6d0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-f036d0961d87d0a5b6d4903a4eaeb6d0"' :
                                            'id="xs-components-links-module-SharedModule-f036d0961d87d0a5b6d4903a4eaeb6d0"' }>
                                            <li class="link">
                                                <a href="components/TagsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TagsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TimestampComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TimestampComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TeamsModule.html" data-type="entity-link">TeamsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TeamsModule-b2428beded8c32a5fd8c14745984e8b2"' : 'data-target="#xs-components-links-module-TeamsModule-b2428beded8c32a5fd8c14745984e8b2"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TeamsModule-b2428beded8c32a5fd8c14745984e8b2"' :
                                            'id="xs-components-links-module-TeamsModule-b2428beded8c32a5fd8c14745984e8b2"' }>
                                            <li class="link">
                                                <a href="components/CreateTeamComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CreateTeamComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TeamsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TeamsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link">UsersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UsersModule-00594c4dc263c3f02d22d530bbcd17ef"' : 'data-target="#xs-components-links-module-UsersModule-00594c4dc263c3f02d22d530bbcd17ef"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UsersModule-00594c4dc263c3f02d22d530bbcd17ef"' :
                                            'id="xs-components-links-module-UsersModule-00594c4dc263c3f02d22d530bbcd17ef"' }>
                                            <li class="link">
                                                <a href="components/DashboardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DashboardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserProfileComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserProfileComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UsersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UsersComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link">AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MessagingService.html" data-type="entity-link">MessagingService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ReviewUnitService.html" data-type="entity-link">ReviewUnitService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TeamsService.html" data-type="entity-link">TeamsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UsersService.html" data-type="entity-link">UsersService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/ServerErrorInterceptor.html" data-type="entity-link">ServerErrorInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuardComponent.html" data-type="entity-link">AuthGuardComponent</a>
                            </li>
                            <li class="link">
                                <a href="guards/ReviewUnitResolver.html" data-type="entity-link">ReviewUnitResolver</a>
                            </li>
                            <li class="link">
                                <a href="guards/SecureInnerPagesGuard.html" data-type="entity-link">SecureInnerPagesGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/CreateReviewUnit.html" data-type="entity-link">CreateReviewUnit</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ReviewUnit.html" data-type="entity-link">ReviewUnit</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ReviewUnitPreview.html" data-type="entity-link">ReviewUnitPreview</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Team.html" data-type="entity-link">Team</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/User.html" data-type="entity-link">User</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});